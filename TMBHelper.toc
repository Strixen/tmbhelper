## Interface: 11306
## Version: 0.0.2
## Title: TMB Tooltips
## Notes: Helper tool for Thats my BIS website.
## Author: Strix
## SavedVariables: ItemListsDB, TMBDB
## OptionalDeps: Ace3
## X-Embeds: Ace3

embeds.xml
core.lua
